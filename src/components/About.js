// React
import React from 'react';

// MaterialUI
import Grid from '@material-ui/core/Grid';

// Styles
import './../styles/About.css';

// Components
import BigText from './../reusable/BigText';

// Photo
import Avatar from './../media/omar_pixel.png'


const About = () => {
    return(
        <div className="about">
            <Grid container>
                <Grid item xs={12} sm={7} className="about content">
                    {/* <BigText
                        textOne={">> say_hi()"}
                    /> */}
                    <BigText
                        textOne={">> Hi, I'm"}
                        textColor={"#1FB0EF"}
                        textTwo={"Edgar Omar Gómez Zárate"}
                    />
                    {/* <BigText
                        textOne={">> degree()"}
                    /> */}
                    <BigText
                        textOne={">>"}
                        textColor={"#FA38A2"}
                        textTwo={"Software Engineer"}
                    />
                    {/* <BigText

                        textOne={">> about()"}
                    /> */}
                    <BigText
                        textOne={">> Full Stack developer. Passionate about technologies and dedicated to develop new tools that help people."}
                    />
                </Grid>
                <Grid item xs={12} sm={5} className="about content">
                    <img src={Avatar} alt="Edgar Omar Gómez Zárate" />
                </Grid>
            </Grid>
        </div>
    );
};

export default About;
