// React
import React from 'react';

// MaterialUI
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';

// Components
import CoCeItemList from './../reusable/CoCeItemList';

// Styles
import './../styles/Course.css'

const Course = () => {

    const leftHeader = ">> Certifications";
    const rightHeader = ">> Courses";

    return(
        <div className="courses-certifications">
            <Grid container>
                <Grid item xs={12} sm={6} className="c-left" >
                    <h2 style={{ color: '#FA38A2', fontSize: "24px", marginBottom: "1em" }}>{rightHeader}</h2>
                    <List component="nav" aria-label="courses">
                        <CoCeItemList
                            key={"DevOps with Gitlab"}
                            title={"DevOps with Gitlab"}
                            dateReached={"Platzi (September 2020)"}
                        />
                        <CoCeItemList
                            key={"Data Engineering"}
                            title={"Data Engineering"}
                            dateReached={"Platzi (November 2019)"}
                        />
                        <CoCeItemList
                            key={"Python"}
                            title={"Python"}
                            dateReached={"Platzi (July 2019)"}
                        />
                        <CoCeItemList
                            key={"DjangoP"}
                            title={"Django"}
                            dateReached={"Platzi (November 2018)"}
                        />
                        <CoCeItemList
                            key={"DjangoUAQ"}
                            title={"Django"}
                            dateReached={"U.A.Q. (November 2018)"}
                        />
                        <CoCeItemList
                            key={"Design Patterns"}
                            title={"Design Patterns"}
                            dateReached={"U.A.Q. (November 2018)"}
                        />
                    </List>
                </ Grid>
                <Grid item xs={12} sm={6} className="c-right" >
                    <h2 style={{ color: '#FA38A2', fontSize: "24px", marginBottom: "1em" }}>{leftHeader}</h2>
                    <List component="nav" aria-label="certifications">
                        <CoCeItemList
                            key={"SCRUM Fundamentals"}
                            title={"SCRUM Fundamentals"}
                            dateReached={"ScrumStudy (May 2019)"}
                        />
                        <CoCeItemList
                            key={"REST API"}
                            title={"REST API"}
                            dateReached={"HackerRank (August 2020)"}
                        />
                        <CoCeItemList
                            key={"Python"}
                            title={"Python"}
                            dateReached={"HackerRank (May 2020)"}
                        />
                    </List>
                </ Grid>
            </ Grid>
        </div>
    );
};

export default Course;
