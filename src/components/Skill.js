// React
import React from 'react';

// MaterialUI
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';

// Styles
import './../styles/Skill.css';

// Components
import SkillListItem from './../reusable/SkillListItem';

const Skills = () => {
    const leftHeader = ">> Technical Skills";
    const rightHeader = ">> Soft Skills";

    return(
        <div className="skill">
            <Grid container>
                <Grid item xs={12} sm={6} className="left" >
                    <h2 style={{ color: '#1FB0EF', fontSize: "24px", marginBottom: "1em" }}>{leftHeader}</h2>
                    <List component="nav" aria-label="technical-skills">
                        <SkillListItem
                            uid={"Python"}
                            skillText={"Python"}
                            shapeColor={"#1FB0EF"}
                            totalShapes={8}
                        />
                        <SkillListItem
                            uid={"Django"}
                            skillText={"Django"}
                            shapeColor={"#1FB0EF"}
                            totalShapes={8}
                        />
                        <SkillListItem
                            uid={"Javascript"}
                            skillText={"Javascript"}
                            shapeColor={"#1FB0EF"}
                            totalShapes={8}
                        />
                        <SkillListItem
                            uid={"React"}
                            skillText={"React"}
                            shapeColor={"#1FB0EF"}
                            totalShapes={8}
                        />
                        <SkillListItem
                            uid={"PostgreSQL"}
                            skillText={"PostgreSQL"}
                            shapeColor={"#1FB0EF"}
                            totalShapes={7}
                        />
                        <SkillListItem
                            uid={"PHP"}
                            skillText={"PHP"}
                            shapeColor={"#1FB0EF"}
                            totalShapes={6}
                        />
                        <SkillListItem
                            uid={"MySQL"}
                            skillText={"MySQL"}
                            shapeColor={"#1FB0EF"}
                            totalShapes={5}
                        />
                    </List>
                </Grid>
                <Grid item xs={12} sm={6} className="right">
                    <h2 style={{ color: '#ED970E', fontSize: "24px", marginBottom: "1em" }}>{rightHeader}</h2>
                    <List component="nav" aria-label="soft-skills">
                        <SkillListItem
                            uid={"Problem Solving"}
                            skillText={"Problem Solving"}
                            shapeColor={"#ED970E"}
                            totalShapes={8}
                        />
                        <SkillListItem
                            uid={"Proactive"}
                            skillText={"Proactive"}
                            shapeColor={"#ED970E"}
                            totalShapes={9}
                        />
                        <SkillListItem
                            uid={"Teamwork"}
                            skillText={"Teamwork"}
                            shapeColor={"#ED970E"}
                            totalShapes={9}
                        />
                        <SkillListItem
                            uid={"Responsible"}
                            skillText={"Responsible"}
                            shapeColor={"#ED970E"}
                            totalShapes={10}
                        />
                        <SkillListItem
                            uid={"Dedication"}
                            skillText={"Dedication"}
                            shapeColor={"#ED970E"}
                            totalShapes={10}
                        />
                        <SkillListItem
                            uid={"Analytical"}
                            skillText={"Analytical"}
                            shapeColor={"#ED970E"}
                            totalShapes={8}
                        />
                        <SkillListItem
                            uid={"Leadership"}
                            skillText={"Leadership"}
                            shapeColor={"#ED970E"}
                            totalShapes={7}
                        />
                    </List>
                </Grid>
            </Grid>
        </div>
    );
};

export default Skills;
