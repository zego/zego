// React
import React from 'react';

// React-Redux
import { connect } from 'react-redux';

// MaterialUi
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Link from '@material-ui/core/Link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExternalLinkAlt, faHeart, faCopyright } from '@fortawesome/free-solid-svg-icons'

// Styles
import './../styles/Contact.css';

// Actions
import { displayModal, sendAPIMail } from './../actions/index';

class Contact extends React.Component{

    state = {
        leftHeaderText: '>> Contact',
        rightHeaderText: '>> Get in touch',
        name: '',
        email: '',
        subject: '',
        message: '',
    };

    validateFields = () => {
        console.log(this.state)
        const nameRegex = /^([a-zA-Z][:blank][a-zA-Z])|^([a-zA-z])/;
        const emailRegex = /[a-z0-9._%+-]+@[a-z0-9.-]+[\\.][a-z]{2,}$/;

        if (this.state.name === '' || this.state.email === '' || this.state.subject === '' || this.state.message === '')
            return ["Empty Fields", "Please fill all the fields in order to send the message"];

        if (!nameRegex.test(this.state.name))
            return ["Invalid Name", "Please enter a valid name in order to send the message"];

        if (!emailRegex.test(this.state.email))
            return ["Invalid Email", "Please enter a valid email in order to send the message"];

        if (this.state.subject.length < 10) 
            return ["Invalid Subject", "Subject must have at least 10 characters or more"];

        if (this.state.message.length < 20)
            return ["Invalid Message", "The message must have at least 20 characters or more"];

        return ["OK", "MESSAGE SENT"];
    }

    sendMail = (event) => {
        event.preventDefault();

        const [messageHeader, messageNody] = this.validateFields();

        if (["Empty Fields", "Invalid Name", "Invalid Email", "Invalid Subject", "Invalid Message"].includes(messageHeader)) {
            this.props.displayModal(messageHeader, messageNody);
            return;
        }

        this.props.sendAPIMail(this.state.name, this.state.email, this.state.subject, this.state.message);

    }

    componentWillReceiveProps(newProps) {
        if (newProps.response !== {}) {
            if (newProps.response.success === true) {
                this.props.displayModal("Message sent!", "Your message was sent to Edgar correctly!");
                this.setState({
                    name: '',
                    email: '',
                    subject: '',
                    message: ''
                })
            } else {
                this.props.displayModal("Error", "Oops something went wrong");
            }
        }
    }

    render() {
        return(
            <div className="contact">
                <Grid container>
                    <Grid item xs={12} sm={6} className="form">
                        <h2 className="header-title">{this.state.leftHeaderText}</h2>
                        <h2 className="header-description">Have a question or want to work together?</h2>
                        <form>
                            <input
                                type="text"
                                placeholder={">> Name"}
                                value={this.state.name}
                                onChange={event => this.setState({ name: event.target.value })}
                            />
                            <input
                                type="text"
                                placeholder={">> Email"}
                                value={this.state.email}
                                onChange={event => this.setState({ email: event.target.value })}
                            />
                            <input
                                type="text"
                                placeholder={">> Subject"}
                                value={this.state.subject}
                                onChange={event => this.setState({ subject: event.target.value })}
                            />
                            <textarea
                                id="message"
                                name="message"
                                placeholder=">> Your message"
                                rows="4"
                                cols="50"
                                value={this.state.message}
                                onChange={event => this.setState({ message: event.target.value })}
                            />
                            <div className="button-container">
                                <input 
                                    type="button"
                                    className="button"
                                    value="Send"
                                    onClick={this.sendMail}
                                />
                            </div>
                        </form>
                    </Grid>
                    <Grid item xs={12} sm={6} className="form" >
                        <h2 className="header-title">{this.state.rightHeaderText}</h2>
                        <List component="nav" aria-label="certifications">
                            <ListItem>
                                <ListItemIcon>
                                    <div className="rectangle" style={{ backgroundColor: "#7CE292" }}></div>
                                </ListItemIcon>
                                <ListItemText
                                    primary={
                                        <React.Fragment>
                                            <p>edgar.omars.goze@gmail.com</p>
                                        </React.Fragment>
                                    }
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <div className="rectangle" style={{ backgroundColor: "#7CE292" }}></div>
                                </ListItemIcon>
                                <ListItemText
                                    primary={
                                        <React.Fragment>
                                            <Link href="https://www.linkedin.com/in/edgaromargomezzarate/" color="inherit" target="_blank" rel="noreferrer">
                                                <span style={{ marginRight: "1em" }}>LinkedIn</span>
                                                <FontAwesomeIcon icon={faExternalLinkAlt} size="sm"  />
                                            </Link>
                                        </React.Fragment>
                                    }
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <div className="rectangle" style={{ backgroundColor: "#7CE292" }}></div>
                                </ListItemIcon>
                                <ListItemText
                                    primary={
                                        <React.Fragment>
                                            <Link href="https://gitlab.com/zego" color="inherit" target="_blank" rel="noreferrer">
                                                <span style={{ marginRight: "1em" }}>Gitlab</span>
                                                <FontAwesomeIcon icon={faExternalLinkAlt} size="sm"  />
                                            </Link>
                                        </React.Fragment>
                                    }
                                />
                            </ListItem>
                            <ListItem style={{ marginBottom: "6em"}}>
                                <ListItemIcon>
                                    <div className="rectangle" style={{ backgroundColor: "#7CE292" }}></div>
                                </ListItemIcon>
                                <ListItemText
                                    primary={
                                        <React.Fragment>
                                            <Link href="https://github.com/EdgarGZ" color="inherit" target="_blank" rel="noreferrer">
                                                <span style={{ marginRight: "1em" }}>Github</span>
                                                <FontAwesomeIcon icon={faExternalLinkAlt} size="sm"  />
                                            </Link>
                                        </React.Fragment>
                                    }
                                />
                            </ListItem>
                        </List>
                    </ Grid>
                    <div className="footer">{"Made with"} <FontAwesomeIcon className="heart" icon={faHeart} style={{ fill: '#e06b75 ' }} size="sm" /> {"by Edgar Gómez"} <FontAwesomeIcon icon={faCopyright} size="sm" /> 2020</div>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        response: state.email
    };
};

export default connect(mapStateToProps, { displayModal, sendAPIMail })(Contact);
