// React
import React from 'react';

// MaterioLUI
import Grid from '@material-ui/core/Grid';

// Components
import CustomCard from './../reusable/CustomCard';

// Styles
import './../styles/Portfolio.css';

// Images
import IAOne from './../media/ia-heart-one.JPG'
import IATwo from './../media/ia-heart-two.JPG'
import IAThree from './../media/ia-heart-three.JPG'
import IAFour from './../media/ia-heart-four.JPG'
import Doceo1 from './../media/doceo-one.JPG';
import Doceo2 from './../media/doceo-two.JPG';
import Doceo3 from './../media/doceo-three.JPG';
import Doceo4 from './../media/doceo-four.JPG';
import Doceo5 from './../media/doceo-five.JPG';
import Doceo6 from './../media/doceo-six.JPG';
import Doceo7 from './../media/doceo-seven.JPG';
import Doceo8 from './../media/doceo-eight.JPG';
import Doceo9 from './../media/doceo-nine.JPG';
import Coop1 from './../media/coop-one.JPG';
import Coop2 from './../media/coop-two.JPG';
import Coop3 from './../media/coop-three.JPG';
import Coop4 from './../media/coop-four.JPG';
import Coop5 from './../media/coop-five.JPG';
import Coop6 from './../media/coop-six.JPG';
import Coop7 from './../media/coop-seven.JPG';
import Coop8 from './../media/coop-eight.JPG';
import Coop9 from './../media/coop-nine.JPG';
import Coop10 from './../media/coop-ten.JPG';
import Inv1 from './../media/inv-one.JPG';
import Inv2 from './../media/inv-two.JPG';
import Inv3 from './../media/inv-three.JPG';
import Inv4 from './../media/inv-four.JPG';
import Inv5 from './../media/inv-five.JPG';
import Inv6 from './../media/inv-six.JPG';
import Inv7 from './../media/inv-seven.JPG';
import Inv8 from './../media/inv-eight.JPG';

const Portfolio = () => {

    const headerTeaxt = ">> Portfolio";

    return(
        <div className='portfolio' >
            <Grid container>
                <Grid item xs={12} >
                    <h2 style={{ color: '#E6DB74', fontSize: "24px", marginBottom: "2em" }}>{headerTeaxt}</h2>
                </Grid>
                <Grid item xs={12} sm={4} style={{ maxWidth: '100%' }} >
                        <CustomCard
                            projectTitle={"Doceo"}
                            projectLink={"https://github.com/EdgarGZ/Doceo"}
                            projectDescription={"A MVC made with Django.\nBrief description: The goal of this system is to help students to improve their notes through tutorships helded by students.\nTechnologies used:\n- Django (Python).\n- JavaScript.\nI did the half of the BackEnd and helped with the FrontEnd data receivement and data display."}
                            imagesArray={[{ label: 'Doceo1', imgPath: Doceo1 }, { label: 'Doceo2', imgPath: Doceo2 }, { label: 'Doceo3', imgPath: Doceo3 }, { label: 'Doceo4', imgPath: Doceo4 }, { label: 'Doceo5', imgPath: Doceo5 }, { label: 'Doceo6', imgPath: Doceo6 }, { label: 'Doceo7', imgPath: Doceo7 }, { label: 'Doceo8', imgPath: Doceo8 }, { label: 'Doceo9', imgPath: Doceo9 }]}
                        />
                    </Grid>
                    <Grid item xs={12} sm={4} >
                        <CustomCard
                            projectTitle={"Cooperandog"}
                            projectLink={"https://github.com/EdgarGZ/Cooperandog"}
                            projectDescription={"Web and mobil App.\nBrief description: The goal of this App is to help people to find their lost pets (mainly dogs and cats).\nTechnologies used:\n- Django and Django Rest Framework (Python).\n- JavaScript.\nI did the BackEnd using django rest famework to develop a REST API. Also I did the Front End logic to handle data with vanilla JS and integrating the Google Maps API for a better UX while using the app."}
                            imagesArray={[{ label: 'Coop1', imgPath: Coop1 }, { label: 'Coop2', imgPath: Coop2 }, { label: 'Coop3', imgPath: Coop3 }, { label: 'Coop4', imgPath: Coop4 }, { label: 'Coop5', imgPath: Coop5 }, { label: 'Coop6', imgPath: Coop6 }, { label: 'Coop7', imgPath: Coop7 }, { label: 'Coop8', imgPath: Coop8 }, { label: 'Coop9', imgPath: Coop9 }, { label: 'Coop10', imgPath: Coop10 }]}
                        />
                    </Grid>
                    <Grid item xs={12} sm={4} >
                        <CustomCard
                            projectTitle={"InventSoft"}
                            projectLink={"https://github.com/EdgarGZ/inventsoft"}
                            projectDescription={"Another MVC made with Django.\nBrief description: The goal of this App is to help stores with their stock.\nTechnologies used:\n- Django (Python).\n- JavaScript.\n- PostgreSQL.\nI did the BackEnd but the challenge was to create my own authentication system such as design pattern implementations. Also I did the Front End logic to handle data with vanilla JS. Last but not least, I didn't use the django's ORM because I implemented a pool connection and created stored procedures to achieve DB's ACID properties."}
                            imagesArray={[{ label: 'Inv1', imgPath: Inv1 }, { label: 'Inv2', imgPath: Inv2 }, { label: 'Inv3', imgPath: Inv3 }, { label: 'Inv4', imgPath: Inv4 }, { label: 'Inv5', imgPath: Inv5 }, { label: 'Inv6', imgPath: Inv6 }, { label: 'Inv7', imgPath: Inv7 }, { label: 'Inv8', imgPath: Inv8 }]}
                        />
                    </Grid>
                    <Grid item xs={12} sm={4} >
                        <CustomCard
                            projectTitle={"I.A. Heart Disease Diagnosis"}
                            projectLink={"https://github.com/EdgarGZ/IA"}
                            projectDescription={"Web system.\nBrief description: The goal of this App is to diagnose a heart disease using diffuse logic.\nTechnologies used:\n- JavaScript.\nI did the BackEnd and the whole logic of this Web App by using diffuse logic and the Max-Min principle. The project is hosted in Github pages (https://edgargz.github.io/IA/)"}
                            imagesArray={[{ label: 'IA-1', imgPath: IAOne }, { label: 'IA-2', imgPath: IATwo }, { label: 'IA-3', imgPath: IAThree }, { label: 'IA-4', imgPath: IAFour }]}
                        />
                    </Grid>
            </Grid>
        </div>
    );
};

export default Portfolio;
