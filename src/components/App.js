// React
import React from 'react';

// Components
import About from './About';
import Experience from './Experience';
import Skill from './Skill';
import Course from './Course';
import Portfolio from './Portfolio';
import Contact from './Contact';
import Modal from './../reusable/Modal';

// MaterialUI
import Grid from '@material-ui/core/Grid';

// Styles
import './../styles/App.css';

const App = () => {
    return(
        <div>
            <Grid container>
                <Grid item xs={12}>
                    <About />
                </Grid>
                <Grid item xs={12}>
                    <Experience />
                </Grid>
                <Grid item xs={12}>
                    <Skill />
                </Grid>
                <Grid item xs={12}>
                    <Course />
                </Grid>
                <Grid item xs={12}>
                    <Portfolio />
                </Grid>
                <Grid item xs={12}>
                    <Contact />
                </Grid>
                <Modal />
            </Grid>
        </div>
    );
};

export default App;
