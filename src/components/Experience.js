// React
import React from 'react';

// Components
import TimeLineItem from './../reusable/TimeLineItem';

// MaterialUI
import Grid from '@material-ui/core/Grid';
import Timeline from '@material-ui/lab/Timeline';

// Styles
import './../styles/Experience.css';

const Experience = () => {
    const headerText = '>> Experience';

    return(
        <div className="experience">
            <Grid container>
                <Grid item xs={12} className="header">
                    {headerText}
                </Grid>
                <Grid item xs={12} >
                    <Timeline className="timeline">
                        <TimeLineItem 
                            company={"DEVELOPMENT CENTER"}
                            position={"Full Stack Developer"}
                            period={"January 2019 - July 2019"}
                            description={"Development and implementation of web-based systems for Centro de Desarrollo using technologies like Python, Django and Javascript. Team leader and responsible of the SDLC using agile metodology SCRUM."}
                            isLastItem={false}
                        />
                        <TimeLineItem 
                            company={"INTERN AT GEIQ"}
                            position={"Software Support Intern"}
                            period={"January 2020 - July 2020"}
                            description={"GEIQ stands for General Electric Infrastructure Queretaro.\nDevelopment and implementation of solutions for process automation for GE Aviation \n- Full Stack development of web-based systems using technologies like Angular framework and PHP. \n- Development of ETLs to automate tasks and improve work yield. \n- Support web-based systems to contribute to the SDLC and help customers improve the quality of their deliverables."}
                            isLastItem={false}
                        />
                        <TimeLineItem 
                            company={"EXPANDIT"}
                            position={"Back End Developer"}
                            period={"July 2020 - October 2020"}
                            description={"Development and implementation of web-based systems for an international project. \nMainly focused in the back-end development:\n- Algorithms to solve problems.\n- TDD and SCRUM to achieve code quality and time delivery.\n- DevOps tasks like CI, code integrity & solve code conflicts.\nAlso I participed as a Front-End developer to support my colleagues. I did some of the most important modules of the systems."}
                            isLastItem={true}
                        />
                    </Timeline>
                </Grid>
            </Grid>
        </div>
    );
};

export default Experience;
