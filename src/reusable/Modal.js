// React
import React from 'react';

// React-Redux
import { connect } from 'react-redux';

// MaterioLUI
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(10, 9, 9, 0.46)'
    },
    paper: {
        backgroundColor: '#17202a',
        minWidth: '30vw',
        minHeight: '30vh',
        // padding: theme.spacing(1, 1, 1),
        border: '1px solid  #f0f3f4',
        borderRadius: '5px'
    },
}));

const TransitionsModal = ({ modalProps }) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [textColor, setTextColor] = React.useState('white');

    React.useEffect(() => {
        if (modalProps.messageHeader !== '' && modalProps.messageBody !== '') {
            if (["Empty Fields", "Invalid Name", "Invalid Email", "Invalid Subject", "Invalid Message"].includes(modalProps.messageHeader)) {
                setTextColor('#ec7063')
            } else {
                setTextColor('#82e0aa')
            }
            
            setOpen(true);
        }
    }, [modalProps]);

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                timeout: 500,
                }}
            >
                <Fade in={open}>
                <div className={classes.paper}>
                    <div style={{ height: '2em', borderRadius: '5px 5px 0px 0px', backgroundColor: '#d0d3d4' }} >
                        <div style={{ float: 'left', color: 'black', backgroundColor: 'inherit', marginTop: '1%', marginLeft: '1%' }}>Console</div>
                        <div onClick={handleClose} style={{ float: 'right', width: '20px', height: '20px', borderRadius: '50%', backgroundColor: '#cb4335', marginTop: '1%', marginRight: '1%' }}></div>
                    </div>
                    <p id="transition-modal-title" style={{ backgroundColor: 'inherit', marginBottom: '2px', color: `${textColor}` }}>{`>> ${modalProps.messageHeader}`}</p>
                    <p id="transition-modal-description" style={{ backgroundColor: 'inherit', borderRadius: '0px 0px 5px 5px', color: `${textColor}` }}>{`>> ${modalProps.messageBody}`}</p>
                </div>
                </Fade>
            </Modal>
        </div>
    );
}

const mapStateToProps = (state) => {
    return { modalProps: state.modal }
}

export default connect(mapStateToProps)(TransitionsModal);
