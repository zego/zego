// React
import React from 'react';

// MaterialUI
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

// Styles
import './../styles/Course.css';

const SkillListItem = ({ title, dateReached }) => {


    return(
        <ListItem>
            <ListItemIcon>
                <div className="c-rectangle" style={{ backgroundColor: "#FA38A2" }}></div>
            </ListItemIcon>
            <ListItemText
                primary={
                    <React.Fragment>
                        <p className="c-text">{title}</p><span className="c-dot">·</span><span className="c-date">{dateReached}</span>
                    </React.Fragment>
                }
            />
        </ListItem>
    );
};

export default SkillListItem;
