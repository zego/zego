// React
import React from 'react';

// MaterialUI
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Link from '@material-ui/core/Link'

// FontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons'

// Styles
import './../styles/Portfolio.css';

// Components
import Swipeable from './../reusable/Swipeable';

const CustomCard = ({ projectTitle, projectLink, projectDescription, imagesArray }) => {
    return(
        <Card className="card" style={{ borderRadius: 0, backgroundColor: 'black' }}>
            <Swipeable
                images={imagesArray}
            />
            <CardHeader
                className="card-header"
                title={
                    <React.Fragment>
                        <h5 className="card-header-title">{projectTitle}</h5>
                    </React.Fragment>
                }
                subheader={
                    <React.Fragment>
                        <Link href={projectLink} color="inherit" target="_blank" rel="noreferrer">
                            <span style={{ marginRight: '1em', fontSize: '12px' }}>{projectTitle}{projectTitle.endsWith('s') ? "'": "'s"} code</span>
                            <FontAwesomeIcon icon={faExternalLinkAlt} size="xs"  />
                    </Link>
                    </React.Fragment>
                }
            />
            <CardContent style={{}}>
                    <React.Fragment>
                        <p className="card-description" style={{ fontSize: "12px" }}>{projectDescription}</p>
                    </React.Fragment>
            </CardContent>
        </Card>
    );
};

export default CustomCard;
