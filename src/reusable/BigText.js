// React
import React from 'react';

// MaterialUI
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(() => ({
    root: {
        fontSize: "32px",
        lineHeight: "40px"
    },
}));

const BigText = ({ textOne, textColor, textTwo, textThree }) => {
    const classes = useStyles();

    return(
        <div className={classes.root}>
            <p>{textOne} <span style={{ color: `${textColor}` }}>{textTwo}</span> {textThree}</p>
        </div>
    );;
};

export default BigText;
