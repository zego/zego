// React
import React from 'react';

// MaterialUI
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

// Styles
import './../styles/Skill.css';

const SkillListItem = ({ uid, skillText, shapeColor, totalShapes }) => {

    const auxiliarArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    const renderRectangles = () => {
        return auxiliarArray.map((element, index) => {
            return index < totalShapes ? 
                <div key={index} className="rectangle" style={{ backgroundColor: `${shapeColor}` }}></div>
            :
                <div key={index} className="rectangle"></div>
        })
    }

    return(
        <ListItem key={uid}>
            <ListItemIcon>
                <div style={{ width: 12, height: 12, backgroundColor: `${shapeColor}` }}></div>
            </ListItemIcon>
            <ListItemText
                primary={
                    <React.Fragment>
                        <p className="text">{skillText}</p> <span className="rank">{renderRectangles()}</span>
                    </React.Fragment>
                }
            />
        </ListItem>
    );
};

export default SkillListItem;
