// React
import React from 'react';

// MaterialUI
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot';

// Styles
import './../styles/Experience.css';

const TimeLineItem = ({ company, position, period, description, isLastItem }) => {
    return(
        <TimelineItem>
            <TimelineSeparator>
                <TimelineDot style={{ width: "0.5em", height: "0.5em" }} />
                {isLastItem ? null : <TimelineConnector style={{ backgroundColor: "#8781FF" }} />}
            </TimelineSeparator>
            <TimelineContent>
                <div>
                    <p className="job-company">
                        {company}
                    </p>
                    <p style={{ height: "20px" }}></p>
                    <p className="job-position">
                        {position} · {period}
                    </p>
                    <p style={{ height: "20px" }}></p>
                    <p className="job-description">
                        {description}
                    </p>
                    <p style={{ height: "20px" }}></p>
                </div>
            </TimelineContent>
        </TimelineItem>
    );
};

export default TimeLineItem;
