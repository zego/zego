// React
import React from 'react';

// MaterialUi
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';

// FontAwesome
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons'

// Swipable
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';


const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const tutorialSteps = [
    {
        label: 'San Francisco – Oakland Bay Bridge, United States',
        imgPath:
        'https://images.unsplash.com/photo-1537944434965-cf4679d1a598?auto=format&fit=crop&w=400&h=250&q=60',
    },
    {
        label: 'Bird',
        imgPath:
        'https://images.unsplash.com/photo-1538032746644-0212e812a9e7?auto=format&fit=crop&w=400&h=250&q=60',
    },
    {
        label: 'Bali, Indonesia',
        imgPath

    :
        'https://images.unsplash.com/photo-1537996194471-e657df975ab4?auto=format&fit=crop&w=400&h=250&q=80',
    },
    {
        label: 'NeONBRAND Digital Marketing, Las Vegas, United States',
        imgPath:
        'https://images.unsplash.com/photo-1518732714860-b62714ce0c59?auto=format&fit=crop&w=400&h=250&q=60',
    },
    {
        label: 'Goč, Serbia',
        imgPath:
        'https://images.unsplash.com/photo-1512341689857-198e7e2f3ca8?auto=format&fit=crop&w=400&h=250&q=60',
    },
];

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 400,
        flexGrow: 1,
        background: 'inherit',
        position: 'relative'
    },
    header: {
        display: 'flex',
        alignItems: 'center',
        height: 50,
        paddingLeft: theme.spacing(4),
    },
    img: {
        maxHeight: 255,
        display: 'block',
        maxWidth: 400,
        overflow: 'hidden',
        width: 'auto',
        height: 'auto',
        padding: 0,
        margin: 'auto'
    },
}));

function Swipeable({ images }) {
    const classes = useStyles();
    const theme = useTheme();
    const [activeStep, setActiveStep] = React.useState(0);
    const maxSteps = images ? images.length : tutorialSteps.length;
    const carrouselImges = images || tutorialSteps;

    // const handleNext = () => {
    //     setActiveStep((prevActiveStep) => prevActiveStep + 1);
    // };

    // const handleBack = () => {
    //     setActiveStep((prevActiveStep) => prevActiveStep - 1);
    // };

    const handleStepChange = (step) => {
        setActiveStep(step);
    };

    return (
        <div className={classes.root}>
            <AutoPlaySwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={activeStep}
                onChangeIndex={handleStepChange}
                enableMouseEvents
            >
                {carrouselImges.map((step, index) => (
                <div key={step.label}>
                    {Math.abs(activeStep - index) <= 2 ? (
                    <img className={classes.img} src={step.imgPath} alt={step.label} />
                    ) : null}
                </div>
                ))}
            </AutoPlaySwipeableViews>
            <MobileStepper
                steps={maxSteps} //DOTS
                position="static"
                // variant="text"
                variant="dots"
                activeStep={activeStep}
                // nextButton={
                //     <button onClick={handleNext} disabled={activeStep === maxSteps - 1} style={{ border: 'none', background: 'none' }}><FontAwesomeIcon icon={faChevronRight} style={{ fill: 'white', background: 'none' }} size="lg" /></button>
                // }
                // backButton={
                //     <button onClick={handleBack} disabled={activeStep === 0} style={{ border: 'none', background: 'none' }}><FontAwesomeIcon icon={faChevronLeft} style={{ fill: 'white', background: 'none' }} size="lg" /></button>
                // }
                // style={{ background: 'none', color: 'rgba(0, 0, 0, 0)', position: 'absolute', width: '100%', top: '50%', padding: 0 }}
                style={{ background: 'none', position: 'absolute', width: '100%', top: '95%', padding: 0 }} // DOTS
            />
        </div>
    );
}

export default Swipeable;