// Redux
import { combineReducers } from 'redux';

const setDisplayModalReducer = (state = { messageHeader: '', messageBody: ''}, action) => {
    if (action.type === 'DISPLAY_MODAL') {
        return action.payload;
    }

    return state;
};

const setMailAPIResponse = (state = {}, action) => {
    switch (action.type) {
        case 'SEND_MAIL':
            return { ...state, ...action.payload };
        default:
            return state;
    }
};

export default combineReducers({
    modal: setDisplayModalReducer,
    email: setMailAPIResponse,
});