// Axios
import axios from 'axios';

// Constants

export default axios.create({
    baseURL: 'https://django-rest-mailer.herokuapp.com/api/mailer',
});
