// React
import React from 'react';
import ReactDOM from 'react-dom';

// React-Redux
import { Provider } from 'react-redux';

// Redux
import { createStore, applyMiddleware } from 'redux';

// Middlewares
import thunk from 'redux-thunk';

// Reducers
import reducers from './reducers';

// Components
import App from './components/App';


const store = createStore(reducers, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.querySelector('#root')
);
