// API call
import mailer from './../apis/mailer';

// Action creator
export const displayModal = (messageHeader, messageBody) => {
    return {
        type: 'DISPLAY_MODAL',
        payload: {
            messageHeader,
            messageBody
        }
    };
};

export const sendAPIMail = (name, email, subject, message) => async dispatch => {
    const response = await mailer.post('/send/mail/', {
            name,
            email,
            subject,
            message
    });

    dispatch({ type: 'SEND_MAIL', payload: response.data });
};
